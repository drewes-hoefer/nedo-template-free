.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt


.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
Nedo Template Free
=============================================================

:Version:
   |release|

:Language:
   en
:Keywords:
	Template

:Copyricht:
	2019
:Authors:
   Frederik Drewes & Christian Höfer
:Email:
   info@nedo-template.de
:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license

   The content of this document is related to TYPO3 CMS,
   a GNU/GPL CMS/Framework available from `typo3.org
   <https://typo3.org/>`_ .


**Sitemap:**

   :ref:`sitemap`

.. toctree::
   :maxdepth: 3
   :hidden:

   Introduction/Index
   Installation/Index
   Editor/Index
   Sitemap

