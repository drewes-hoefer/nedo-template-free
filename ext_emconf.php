<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "myextensionkey".
 *
 * Auto generated 31-10-2018 19:55
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Nedo Basis Template',
	'description' => 'Template um Inhalte darzustellen',
	'category' => 'misc',
	'shy' => 0,
	'version' => '0.0.3',
	'dependencies' => 'cms,extbase,flux',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'experimental',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 1,
	'lockType' => '',
	'author' => 'Frederik Drewes',
	'author_email' => 'info@frederik-drewes.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '8.0.0-9.5.9',
			'cms' => '',
			'extbase' => '',
			'flux' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:0:{}',
	'suggests' => array(
	),
);
