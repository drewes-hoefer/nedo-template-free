$(document).ready(function () {
	console.log('jquery loaded');

	$('#rwd-toggle').click(function () {
		$('#navigation').toggleClass('showNav');
	});

	$(window).scroll(function () {
		if($(window).scrollTop() > 800) {
			$('#up').fadeIn("slow");
			$('#header').addClass('sticky-header');
		} else {
			$('#up').fadeOut("slow");
			$('#header').removeClass('sticky-header');
		}
	});

	$('#up').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 1000);
	});

	var gallerys = $('.ce-gallery');
	$.each(gallerys, function (index) {
		if($(this).attr('data-ce-columns') > 1) {
			var col = 'gallery-' + $(this).attr('data-ce-columns');
			$(this).addClass(col);
			$(this).addClass('gallery');
		}
	});

	$('.mainnav li a').click(function (e) {
		e.preventDefault();
		var href = this.href;
		var target = $(href.substring(href.lastIndexOf('#')));
		if( target.length ) {
			$('html, body').animate({
				scrollTop: $(target).offset().top
			}, 1000);
		}
	});

	$('.btn').tooltip()

});