.. include:: ../Includes.txt


.. _for-editors:

===========
Links
===========

Target group: **Editors**

If you want to use the link element you have to choose a page, where you want to place the element.
Click on the button "+ content", navigate to "Speziale_Elemente" and choose "Link Element".

.. figure:: ../Images/Backend/content/link-element.png
   :class: with-shadow
   :alt: link element

Inside the settings of the link element you have two settings tabs for this element.

* Button Link
  Here you define the linktext and the link itself.
* Styling
  Here you can choose the predefined colors.


