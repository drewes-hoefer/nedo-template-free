.. include:: ../Includes.txt


.. _for-editors:

===========
icons
===========

Target group: **Editors**

The icon element is intended to display text with a icon, provided as image.
As in every element you have several settings to fill out if you want to use this element.

Choose an site where you want to place the icon element and click on the button "+ content".
Under the tab "Speziale_Elemente" you will find a element "Icon Element".

.. figure:: ../Images/Backend/content/choose-icon.png
   :class: with-shadow
   :alt: choose icon element

Settings:

First of all you can set a headline for the icon element.
After that you can fill out the "Inhalt (content)" section for the element.

.. figure:: ../Images/Backend/content/icon-settings-1.PNG
   :class: with-shadow
   :alt: icon settings

Beneath that you can select an image which will be displayed as icon.

.. figure:: ../Images/Backend/content/icon-settings-2-image.PNG
   :class: with-shadow
   :alt: icon settings

Thereafter you can set an link and linktext, if you do not set those they won't get displayed.
The last three options are settings for the icon element behaviour.

.. figure:: ../Images/Backend/content/icon-settings-3.PNG
   :class: with-shadow
   :alt: icon settings

* "Icons verhalten (Icon behaviour):
  "Standard:"
  Text beneath Icon image
  "Horizontal":
  Text beside Icon

* Ausrichtung (Alignment): alignment of the text inside the div.
  Links (Left)
  Mitte (center)
  Rechts (right)

* Link verhalten (Link behaviour):
  Rund (round button)
  Button (not round button)
