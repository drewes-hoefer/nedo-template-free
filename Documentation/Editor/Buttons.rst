.. include:: ../Includes.txt


.. _for-editors:

===========
Buttons
===========

Target group: **Editors**

If you want to use the button element you have to choose a page, where you want to place the element.
Click on the button "+ content", navigate to "Speziale_Elemente" and choose "Button Element".

.. figure:: ../Images/Backend/content/button-element.png
   :class: with-shadow
   :alt: button element

Inside the settings of the link element you have two settings tabs for this element.

* Button Link
  Here you define the linktext and the link itself.
* Styling
  Here you can choose several settings:
  - Button Größe:
    Klein (small)
    Mittel (middle)
    Groß (Big)

  - Button Style:
    Rund (round)
    Simple (simple)

  - Farben:
    - background color of button

   - Button Outline:
     no color filling, but border with color.


