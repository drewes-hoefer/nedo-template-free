<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/setup.txt">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tpl_nedofree/Configuration/TSConfig/pageTSConfig/BackendLayouts/WithSidebar.tsconfig">

plugin.tx_tplnedofree{
	view {
		templateRootPaths.0 = {$plugin.Typo: tplnedofree.view.templateRootPaths.default}
		partialRootPaths.0 = {$plugin.Typo: tplnedofree.view.partialRootPaths.default}
		layoutRootPaths.0 = {$plugin.Typo: tplnedofree.view.layoutRootPaths.default}
	}
}

plugin.tx_form {
    settings {
        yamlConfigurations {
            100 = EXT:tpl_nedofree/Configuration/Form/CustomFormSetup.yaml
        }
    }
}

page = PAGE
page {
   typeNum = 0

		10 = FLUIDTEMPLATE
        10 {
      		templateName = TEXT
      		templateName.stdWrap.cObject = CASE
      		templateName.stdWrap.cObject {
      		   key.data = pagelayout

      		   pagets__global_template_default = TEXT
      		   pagets__global_template_default.value = Default

      		   default = TEXT
      		   default.value = Default
      		}
      		templateRootPaths {
              0 = EXT:tpl_nedofree/Resources/Private/Templates/Page/
              1 = {$page.fluidtemplate.templateRootPath}
            }
            partialRootPaths {
              0 = EXT:tpl_nedofree/Resources/Private/Partials/Page/
              1 = {$page.fluidtemplate.partialRootPath}
            }
            layoutRootPaths {
              0 = EXT:tpl_nedofree/Resources/Private/Layouts/
              1 = {$page.fluidtemplate.layoutRootPath}
            }
            variables {
            	footerPid = TEXT
            	footerPid.value = {$plugin.tx_tplnedofree.footerPid}
            }
      	}

}

config {
   no_cache = 1
   compressCss = 0
   compressJs = 0
   concatenateCss = 0
   concatenateJs = 0
   content_from_pid_allowOutsideDomain = 1
}

