.. include:: ../Includes.txt



.. _installation:

============
First steps
============

Target group: **Administrators**

After activating the Extension it is necessary to include it in the page template.

To do that edit the page template via "Template".

.. figure:: ../Images/Backend/template-einstellungen.png
   :class: with-shadow
   :alt: modify templates

Click on the button "Edit the whole template record". Navigate to the tab "Includes" and click on "nedotemplate" in the "Available Items" column and save the settings.

.. figure:: ../Images/Backend/template-einstellungen-1.png
   :class: with-shadow
   :alt: modify templates

After that the template is installed an ready to use.

The next chapter introduces how to use backend layouts and content elements implemented in the template.

`Use Backend Layouts <Editor/Index.html>`__
