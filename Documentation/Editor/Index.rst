.. include:: ../Includes.txt


.. _for-editors:

===========
For Editors
===========

Target group: **Editors**

How to use the extension from the perspective of an editor.

After installation and configuration it is easy to use the template.

If your want to use the template in a page you firstly have to create a page or edit a existing page.

Navigate to the page modul, click on a page in the page tree and click on the edit page button.

.. figure:: ../Images/Backend/edit-page.png
   :class: with-shadow
   :alt: edit page

Inside the edit screen, navigate to the tab called "Appearance":

.. figure:: ../Images/Backend/edit-page-appearance.png
   :class: with-shadow
   :alt: edit page

You will find a dropdown inside the appearance tab. Here you have several options to choose from.

* Einspaltig											:translation -> One Column Site
* Zweispaltig (Seitenleiste links)						:translation -> Two Column Site (Sidebar left)
* Zweispaltig (Seitenleiste rechts)						:translation -> Two Column Site (Sidebar right)
* Dreispaltig											:translation -> Three Column Site
* Startseite											:translation -> Frontpage Layout
* Seite mit Hintergrundbild								:translation -> Site with full size background image

We choose "Einspaltig" for this documentation.
After choosing your backend layout, saving and closing the edit page screen, you will notice a change in the display of the page in the backend.

.. figure:: ../Images/Backend/page-with-backend-layout.PNG
   :class: with-shadow
   :alt: show page backend

There are two areas where you can add content.
The first is called "banner". This is the area where you can put an banner element with an background image and optionally content inside.
The second is responsible for the main content area. This is the place where you put all your content between the banner/head and the footer.

The next image shows the sections from the backend on the frontend.

.. figure:: ../Images/Frontend/full-size.png
   :class: with-shadow
   :alt: frontend rendered with sections

There are several content Elements from which to choose from. The next chapters are about them:

* `Cards <Cards.html>`__
* `Icons <Icons.html>`__
* `Links <Links.html>`__
* `Buttons <Buttons.html>`__
