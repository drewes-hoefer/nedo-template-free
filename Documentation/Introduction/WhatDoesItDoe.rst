.. include:: ../Includes.txt


.. _introduction:

============
What does it do?
============

The 'Nedo Template Free' provides an easy to install Template.
It is based on fluid, flux an vhs which are dependencies for this template.

The template has complete HTML & CSS Elements such as 'card Element" ready to use.

You can use the template for landingpages, websites and more, without the need of coding one line.

