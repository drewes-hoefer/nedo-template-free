.. include:: ../Includes.txt



.. _installation:

============
Installation
============

Target group: **Administrators**

You can install the extension via the TYPO3 extension manager or via the git clone command.

If you want to install the template via git, visit our git `respository <https://gitlab.com/drewes-hoefer/nedo-template-free`__

If you want to install the template via the TYPO3 Extension Manager, login to your TYPO3 backend, navigate to "Extensions" and search for "nedo".
Install the template via the "Download" Button.

* `First steps <Integration.html>`__



