<?php
/**
 * Created by PhpStorm.
 * User: Frederik Drewes
 * Date: 11.07.2019
 * Time: 10:49
 */

namespace Basic\TplBasic\ViewHelpers;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;


class LogoViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * Children must not be escaped, to be able to pass {bodytext} directly to it
     *
     * @var bool
     */
    protected $escapeChildren = false;

    /**
     * @var bool
     */
    protected $escapeOutput = false;


    /**
     * Initialize arguments.
     *
     * @throws \TYPO3Fluid\Fluid\Core\ViewHelper\Exception
     */
    public function initializeArguments() {
        parent::initializeArguments();
        //        $this->registerArgument('field', 'text', 'Farben Einstellungen', true, '');
    }

    /**
     * ViewHelper um die Farbe aus den Seiteneigenschaften zu erhalten.
     *
     * @return string
     */
    public function render() {
        $settings = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['tpl_nedofree'];
        if(!empty($settings['logoImage'])) {
            return '<img src="/fileadmin'.$settings['logoImage'].'" />';
        }
        return $settings['logoText'];
    }

}