mod.web_layout.BackendLayouts {
    Einspaltig {
        title = Einspaltig
        icon = fileadmin/tpl_nedofree/Resources/Public/Bilder/einspaltig.png
        config {
            backend_layout {
                colCount = 1
                rowCount = 2
                rows {
                    1 {
                        columns {
                            1 {
                                name = Banner
                                colPos = 1
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = Hauptinhalt
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }

    Zweispaltig {
        title = Zweispaltig (Seitenleiste Links)
        icon = fileadmin/tpl_nedofree/Resources/Public/Bilder/zweispaltig-links.png
        config {
            backend_layout {
                colCount = 3
                rowCount = 2
                rows {
                    1 {
                        columns {
                            1 {
                                name = Banner
                                colPos = 2
                                colspan = 4
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = Linke Seitenleiste
                                colPos = 1
                                colspan = 1
                            }
                            2 {
                                name = Hauptinhalt
                                colspan = 3
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }

    Zweispaltig_1 {
        title = Zweispaltig (Seitenleiste Rechts)
        icon = fileadmin/tpl_nedofree/Resources/Public/Bilder/zweispaltig.png
        config {
            backend_layout {
                colCount = 3
                rowCount = 2
                rows {
                    1 {
                        columns {
                            1 {
                                name = Banner
                                colPos = 2
                                colspan  = 4
                            }
                        }
                    }
                    2 {
                        columns {
                            2 {
                                name = Rechte Seitenleiste
                                colPos = 1
                                colspan = 1
                            }
                            1 {
                                name = Hauptinhalt
                                colspan = 3
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }

    Dreispaltig {
        title = Dreispaltig
        icon = fileadmin/tpl_nedofree/Resources/Public/Bilder/dreispaltig.png
        config {
            backend_layout {
                colCount = 4
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Linke Seitenleiste
                                colPos = 1
                            }
                            2 {
                                name = Hauptinhalt
                                colspan = 2
                                colPos = 0
                            }
                            3 {
                                name = Rechte Seitenleiste
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }
    }
    Startseite {
        title = Startseite
        icon = fileadmin/tpl_nedofree/Resources/Public/Bilder/dreispaltig.png
        config {
            backend_layout {
                colCount = 1
                rowCount = 3
                rows {
                    1 {
                        columns {
                            1 {
                                name = Kopfbereich
                                colspan = 4
                                colPos = 0
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = Hauptinhalt
                                colspan = 4
                                colPos = 1
                            }
                        }
                    }
                    3 {
                        columns {
                            1 {
                                name = Fußbereich
                                colspan = 4
                                colPos = 2
                            }
                        }
                    }
                }
            }
        }
    }
    Hintergrundbild {
        title = Seite mit Hintergrundbild
        icon = fileadmin/tpl_nedofree/Resources/Public/Bilder/einspaltig.png
        config {
            backend_layout {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Inhaltsbereich
                                colspan = 4
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }
}