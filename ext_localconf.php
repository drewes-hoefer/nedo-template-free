<?php
/**
 * Created by PhpStorm.
 * User: hom1
 * Date: 01.11.18
 * Time: 00:43
 */


if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

call_user_func(
    function ($extConfString) {

        // Add pageTS config
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tpl_nedofree/Configuration/TSConfig/pageTSConfig/BackendLayouts/tsconfig.ts">');

    },$_EXTCONF
);

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['default'] = 'EXT:tpl_nedofree/Configuration/TSConfig/pageTSConfig/Rte/RTE.yaml';


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Fd.TplNedofree', 'Page');
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Fd.TplNedofree', 'Content');

